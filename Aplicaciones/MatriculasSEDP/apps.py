from django.apps import AppConfig


class MatriculassedpConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicaciones.MatriculasSEDP'
