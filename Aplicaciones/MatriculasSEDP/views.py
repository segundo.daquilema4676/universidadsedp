from django.shortcuts import render, redirect
from django.core.files.storage import default_storage
from django.conf import settings
from django.contrib import messages
from .models import CarreraSEDP
from .models import CursoSEDP
from .models import AsignaturaSEDP
from django.core.mail import send_mail
from django.http import HttpResponseRedirect
# Create your views here.
def listadoCarreras(request):
    carrerasBdd = CarreraSEDP.objects.all()
    return render(request, 'listadoCarreras.html', {'carreras': carrerasBdd})

def guardarCarrera(request):
    if request.method == 'POST':
        nombre_carrera = request.POST.get("nombre_carrera")
        director_carrera = request.POST.get("director_carrera")
        perfil_egreso = request.POST.get("perfil_egreso")
        duracion_carrera = request.POST.get("duracion_carrera")

        logo_carrera = request.FILES.get("logo_carrera")

        carrera = CarreraSEDP.objects.create(
            nombreCarreraSEDP=nombre_carrera,
            directorCarreraSEDP=director_carrera,
            perfilEgresoCarrera=perfil_egreso,
            duracionCarrera=duracion_carrera,
            logoCarreraSEDP=logo_carrera
        )

        messages.success(request, 'CARRERA GUARDADA EXITOSAMENTE')
        return redirect('/')

def eliminarCarrera(request, id_carrera):
    try:
        carrera_eliminar = CarreraSEDP.objects.get(idCarreraSEDP=id_carrera)
        carrera_eliminar.delete()
        messages.success(request, 'CARRERA ELIMINADA EXITOSAMENTE')
    except CarreraSEDP.DoesNotExist:
        messages.error(request, 'La carrera no existe')

    return redirect('/')

def editarCarrera(request, id_carrera):
    try:
        carrera_editar = CarreraSEDP.objects.get(idCarreraSEDP=id_carrera)
        carreras_bdd = CarreraSEDP.objects.all()
        return render(request, 'editarCarrera.html', {'carrera': carrera_editar, 'carreras': carreras_bdd})
    except CarreraSEDP.DoesNotExist:
        messages.error(request, 'La carrera no existe')
        return redirect('/')

def procesarActualizacionCarrera(request):
    if request.method == 'POST':
        id_carrera = request.POST.get("id_carrera")
        nombre_carrera = request.POST.get("nombre_carrera")
        director_carrera = request.POST.get("director_carrera")
        perfil_egreso = request.POST.get("perfil_egreso")
        duracion_carrera = request.POST.get("duracion_carrera")

        logo_carrera = request.FILES.get('logo_carrera')

        try:
            carrera_editar = CarreraSEDP.objects.get(idCarreraSEDP=id_carrera)

            if logo_carrera is not None:
                carrera_editar.logoCarreraSEDP = logo_carrera

            carrera_editar.nombreCarreraSEDP = nombre_carrera
            carrera_editar.directorCarreraSEDP = director_carrera
            carrera_editar.perfilEgresoCarrera = perfil_egreso
            carrera_editar.duracionCarrera = duracion_carrera
            carrera_editar.save()

            messages.success(request, 'CARRERA ACTUALIZADA EXITOSAMENTE')
        except CarreraSEDP.DoesNotExist:
            messages.error(request, 'La carrera no existe')

    return redirect('/')

def listadoCursos(request):
    cursosBdd = CursoSEDP.objects.all()
    carrerasBdd = CarreraSEDP.objects.all()
    return render(request, 'listadoCursos.html', {'cursos': cursosBdd, 'carreras': carrerasBdd})


def eliminarCurso(request, idCursoSEDP):
    cursoEliminar = CursoSEDP.objects.get(idCursoSEDP=idCursoSEDP)
    cursoEliminar.delete()
    messages.success(request, 'CURSO ELIMINADO EXITOSAMENTE')
    return redirect('/listadoCursos/')


def guardarCurso(request):
    nivelCursoSEDP = request.POST["nivelCursoSEDP"]
    descripcionCursoSEDP = request.POST["descripcionCursoSEDP"]
    aulaCursoSEDP = request.POST["aulaCursoSEDP"]
    modalidadCursoSEDP = request.POST["modalidadCursoSEDP"]
    promedioCursoSEDP = request.POST["promedioCursoSEDP"]
    carreraSEDP_id = request.POST["carreraSEDP"]

    # Insertando datos mediante el ORM de Django
    curso = CursoSEDP.objects.create(
        nivelCursoSEDP=nivelCursoSEDP,
        descripcionCursoSEDP=descripcionCursoSEDP,
        aulaCursoSEDP=aulaCursoSEDP,
        modalidadCursoSEDP=modalidadCursoSEDP,
        promedioCursoSEDP=promedioCursoSEDP,
        carreraSEDP_id=carreraSEDP_id,
    )

    messages.success(request, 'CURSO GUARDADO EXITOSAMENTE')
    return redirect('/listadoCursos/')


def editarCurso(request, idCursoSEDP):
    carrerasBdd = CarreraSEDP.objects.all()
    cursoEditar = CursoSEDP.objects.get(idCursoSEDP=idCursoSEDP)
    return render(request, 'editarCurso.html', {'curso': cursoEditar, 'carreras': carrerasBdd})


def procesarActualizacionCurso(request):
    # Obtener los datos del formulario
    idCursoSEDP = request.POST["idCursoSEDP"]
    nivelCursoSEDP = request.POST["nivelCursoSEDP"]
    descripcionCursoSEDP = request.POST["descripcionCursoSEDP"]
    aulaCursoSEDP = request.POST["aulaCursoSEDP"]
    modalidadCursoSEDP = request.POST["modalidadCursoSEDP"]
    promedioCursoSEDP = request.POST["promedioCursoSEDP"]
    carreraSEDP_id = request.POST["carreraSEDP"]

    # Actualizar el curso con los nuevos datos
    cursoEditar = CursoSEDP.objects.get(idCursoSEDP=idCursoSEDP)
    cursoEditar.nivelCursoSEDP = nivelCursoSEDP
    cursoEditar.descripcionCursoSEDP = descripcionCursoSEDP
    cursoEditar.aulaCursoSEDP = aulaCursoSEDP
    cursoEditar.modalidadCursoSEDP = modalidadCursoSEDP
    cursoEditar.promedioCursoSEDP = promedioCursoSEDP
    cursoEditar.carreraSEDP_id = carreraSEDP_id

    cursoEditar.save()

    messages.success(request, 'CURSO ACTUALIZADO EXITOSAMENTE')
    return redirect('/listadoCursos/')

def listadoAsignaturas(request):
    cursosBdd = CursoSEDP.objects.all()
    asignaturas = AsignaturaSEDP.objects.all()
    return render(request, 'listadoAsignaturas.html', {'asignaturas': asignaturas, 'cursos': cursosBdd})

def eliminarAsignatura(request, idAsignaturaSEDP):
    asignatura_eliminar = AsignaturaSEDP.objects.get(idAsignaturaSEDP=idAsignaturaSEDP)
    asignatura_eliminar.delete()
    messages.success(request, 'Asignatura eliminada exitosamente')
    return redirect('/listadoAsignaturas/')

def guardarAsignatura(request):
    if request.method == 'POST':
        # Retrieve data from the form
        nombre = request.POST['nombre']
        credito = request.POST['credito']
        fecha_inicio = request.POST['fecha_inicio']
        fecha_finalizacion = request.POST['fecha_finalizacion']
        profesor = request.POST['profesor']
        horario = request.POST['horario']
        horas_total = request.POST['horas_total']
        curso_id = request.POST['curso_id']

        # Handling file upload
        silabo = request.FILES.get('silabo')



        # Create new AsignaturaSEDP object
        asignatura = AsignaturaSEDP.objects.create(
            nombreAsignaturaSEDP=nombre,
            creditoAsignaturaSEDP=credito,
            fechaInicioAsignaturaSEDP=fecha_inicio,
            fechaFinalizacionAsignaturaSEDP=fecha_finalizacion,
            profesorAsignaturaSEDP=profesor,
            silaboAsignaturaSEDP=silabo,
            horarioAsignaturaSEDP=horario,
            horasTotalAsignaturaSEDP=horas_total,
            cursoSEDP_id=curso_id
        )

        messages.success(request, 'Asignatura guardada exitosamente')
        return redirect('/listadoAsignaturas/')
    else:
        # Handle GET request (if applicable)
        pass

def editarAsignatura(request, idAsignaturaSEDP):
    cursosBdd = CursoSEDP.objects.all()
    asignatura_editar = AsignaturaSEDP.objects.get(idAsignaturaSEDP=idAsignaturaSEDP)
    return render(request, 'editarAsignatura.html', {'asignatura': asignatura_editar, 'cursos': cursosBdd})

def procesarActualizacionAsignatura(request):
    if request.method == 'POST':
        # Retrieve data from the form
        id_asignatura = request.POST['id_asignatura']
        nombre = request.POST['nombre']
        credito = request.POST['credito']
        fecha_inicio = request.POST['fecha_inicio']
        fecha_finalizacion = request.POST['fecha_finalizacion']
        profesor = request.POST['profesor']
        horario = request.POST['horario']
        horas_total = request.POST['horas_total']
        curso_id = request.POST['curso_id']

        # Handling file upload
        if 'silabo' in request.FILES:
            silabo = request.FILES['silabo']
            # Save the file to the filesystem and update the path in the model
            # You can follow a similar approach to the one used in your original code

        # Update existing AsignaturaSEDP object
        asignatura_editar = AsignaturaSEDP.objects.get(idAsignaturaSEDP=id_asignatura)
        asignatura_editar.nombreAsignaturaSEDP = nombre
        asignatura_editar.creditoAsignaturaSEDP = credito
        asignatura_editar.fechaInicioAsignaturaSEDP = fecha_inicio
        asignatura_editar.fechaFinalizacionAsignaturaSEDP = fecha_finalizacion
        asignatura_editar.profesorAsignaturaSEDP = profesor
        asignatura_editar.horarioAsignaturaSEDP = horario
        asignatura_editar.horasTotalAsignaturaSEDP = horas_total
        asignatura_editar.cursoSEDP_id = curso_id

        asignatura_editar.save()

        messages.success(request, 'Asignatura actualizada exitosamente')
        return redirect('/listadoAsignaturas/')
    else:
        # Handle GET request (if applicable)
        pass

def vista1(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)
        messages.success(request, 'Se ha enviado tu correo')
        return HttpResponseRedirect('/vista1')

    return render(request, 'enviar_correo.html')
