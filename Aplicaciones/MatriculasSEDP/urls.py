from django.contrib import admin
from django.urls import path, include
from . import views
app_name = 'hospitales'
urlpatterns = [
    path('', views.listadoCarreras),
    path('guardarCarrera/', views.guardarCarrera),
    path('eliminarCarrera/<id_carrera>/', views.eliminarCarrera),
    path('editarCarrera/<id_carrera>/', views.editarCarrera),
    path('procesarActualizacionCarrera/', views.procesarActualizacionCarrera),
    path('listadoCursos/', views.listadoCursos),
    path('guardarCurso/', views.guardarCurso),
    path('eliminarCurso/<idCursoSEDP>/', views.eliminarCurso),
    path('editarCurso/<idCursoSEDP>/', views.editarCurso),
    path('procesarActualizacionCurso/', views.procesarActualizacionCurso),
    path('listadoAsignaturas/', views.listadoAsignaturas, name='listado_asignaturas'),
    path('guardarAsignatura/', views.guardarAsignatura, name='guardar_asignatura'),
    path('eliminarAsignatura/<idAsignaturaSEDP>/', views.eliminarAsignatura, name='eliminar_asignatura'),
    path('editarAsignatura/<idAsignaturaSEDP>/', views.editarAsignatura, name='editar_asignatura'),
    path('procesarActualizacionAsignatura/', views.procesarActualizacionAsignatura, name='procesar_actualizacion_asignatura'),
    path('vista1/', views.vista1, name='vista1'),  # Use 'name' to set the namespace
    path('enviar_correo/', views.enviar_correo, name='enviar_correo'),  # Use 'name' to set the namespace



]
