from django.db import models

# Create your models here.
class CarreraSEDP(models.Model):
    idCarreraSEDP = models.AutoField(primary_key=True)
    nombreCarreraSEDP = models.CharField(max_length=100)
    directorCarreraSEDP = models.CharField(max_length=100)
    logoCarreraSEDP = models.ImageField(upload_to='carrera_logos')
    perfilEgresoCarrera = models.TextField()
    duracionCarrera = models.IntegerField()
    def __str__(self):
        return self.nombreCarreraSEDP

class CursoSEDP(models.Model):
    idCursoSEDP = models.AutoField(primary_key=True)
    nivelCursoSEDP = models.CharField(max_length=50)
    descripcionCursoSEDP = models.TextField()
    aulaCursoSEDP = models.CharField(max_length=50)
    modalidadCursoSEDP = models.CharField(max_length=50)
    promedioCursoSEDP = models.DecimalField(max_digits=5, decimal_places=2)
    carreraSEDP = models.ForeignKey(CarreraSEDP, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.descripcionCursoSEDP

class AsignaturaSEDP(models.Model):
    idAsignaturaSEDP = models.AutoField(primary_key=True)
    nombreAsignaturaSEDP = models.CharField(max_length=100)
    creditoAsignaturaSEDP = models.IntegerField()
    fechaInicioAsignaturaSEDP = models.DateField()
    fechaFinalizacionAsignaturaSEDP = models.DateField()
    profesorAsignaturaSEDP = models.CharField(max_length=100)
    silaboAsignaturaSEDP = models.FileField(upload_to='silabos')
    horarioAsignaturaSEDP = models.CharField(max_length=100)
    horasTotalAsignaturaSEDP = models.IntegerField()
    cursoSEDP = models.ForeignKey(CursoSEDP, on_delete=models.PROTECT, null=True, blank=True)

    def __str__(self):
        return self.nombreAsignaturaSEDP
